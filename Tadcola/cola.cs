﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tadcola
{
    class Cola
    {
        public nodo primero; // inicio cola
        public nodo ultimo; // final de la cola

        public Cola ()
            {
            primero = ultimo = null;
            }
        public void Encolar(int valor)
        {
            nodo aux = new nodo();
            aux.info = valor;
            if (primero == null)//cola vacia
            {
                primero = ultimo = aux; //ingresar dato
                aux.sgte = null; // muestra null
            }
            else
            {
                ultimo.sgte = aux; // ultimo elemento mostrado por aux
                aux.sgte = null;
                ultimo = aux; //meter nodo a la cola 
            }
        }
        public void Desencolar ()
            {
            if (primero == null) Console.WriteLine("cola vacia ");
            else primero = primero.sgte;
            }
        public int DesencolarValor()
        {
            int valor = 0;
            if (primero == null) Console.WriteLine("cola vacia");
            else
            {
                valor = primero.info;
                primero = primero.sgte;
            }
            return valor;
        }
         public void Mostrar ()
        {
            if (primero == null) Console.WriteLine("cola vacia");
            else
            {
                nodo puntero;
                puntero = primero;

                do
                {
                    Console.WriteLine("{0}\t", puntero.info);
                    puntero = puntero.sgte;

                }
                while (puntero != null);
               
                }
            Console.WriteLine("\n");

        }
    }
}

