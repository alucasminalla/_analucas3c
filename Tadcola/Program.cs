﻿using System;
using Tadcola;

namespace Tadcola
{
    class Program
    {
        static void Main(string[] args)
        {
            Cola objcola = new Cola();
            Console.WriteLine("colocando 5 elementos en la cola");
            objcola.Encolar(3);
            objcola.Encolar(2);
            objcola.Encolar(4);
            objcola.Encolar(1);
            objcola.Encolar(2);
            objcola.Mostrar();

            Console.WriteLine("retirando dos elementos de la cola");
            objcola.Desencolar();
            objcola.Mostrar();
            objcola.Desencolar();
            objcola.Mostrar();

            Console.WriteLine("se va a retirar un nodo mas, con el valor de  {0}",objcola.DesencolarValor());
            objcola.Mostrar();
            Console.ReadLine();

        }   
    }
}
