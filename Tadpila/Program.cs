﻿using System;

namespace Tadpila
{
    class Program
    {
        static void Main(string[] args)
        {
            string cadena;
            char caracter;
            pila mipila = new pila();
            pila pilafrase = new pila();
            // Parte I ingreso de carateres de manera individual a la pila y luego se muestran 
            Console.WriteLine("\t\t---Parte I---");
            mipila.Push('a');
            mipila.Push('b');
            mipila.Push('c');
            mipila.Push('d');
            // muestra el contenido de la pila 
            mipila.mostrar();
            Console.ReadLine();
            // se saca un  elemento de la pila 
            mipila.Pop();
            mipila.mostrar();
            Console.ReadLine();
            // Parte II
            // se ingresa una cadena y se ingres todos los carecteres de cicha cadena 
            Console.WriteLine("\t\t---Parte II---");
            Console.WriteLine("Ingrese la palabra: ");
            cadena = Console.ReadLine();
            for (int i=0; i <cadena.Length; i++)
            {
                caracter = char.Parse(cadena.Substring(i, 1));
                pilafrase.Push(caracter);
            } // imprimir contenido de la pila 
            pilafrase.mostrar();
            Console.ReadLine();
        }
    }
}

