﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tadpila
{
    class pila
    {
        public nodo tope;
        public pila ()
        {
            tope = null;
        }
        public void Push (char valor)
        {
            nodo aux = new nodo();
            aux.info = valor;
            //pila vacia 
            if (tope == null) 
            {
                tope = aux;
                aux.sgte = null;
            }

             
            else
            {
                aux.sgte = tope;
                tope = aux;

            }
        }
        public void mostrar()
        {
            nodo puntero; puntero = tope;
            Console.WriteLine("{0}", puntero.info);
            while(puntero.sgte !=null)
            {
                puntero = puntero.sgte;
                Console.WriteLine("{0}", puntero.info);
            }
        }
        public char Pop()
        {
            char valor = ' ';
            if (tope == null)
                Console.WriteLine("Pilad vacia");
            else
            {
                // se elimina el ultimo en entrar y se retorna el valor que se elimina 
                valor = tope.info;
                tope = tope.sgte;
            }
            return valor;
        }
             
    }
}
