﻿using System;

namespace AnaLucas3C
{
    public class Empleado
    {
        private string apellido;
        private string nombre;
        private int edad;
        private string departamento;
        
        public Empleado(string apellido, string nombre, string departamento)
        {
            this.apellido = apellido;
            this.nombre = nombre;
            this.departamento = departamento;

        }

        public string Getapellido()
        {
            return apellido;
        }
        public void Setapellido(string apellido)
        {
            this.apellido = apellido;
        }
        public string Getnombre()
        {
            return nombre;
        }
        public void Setnombre (string nombre)
        {
            this.nombre = nombre;
        }
        public int Getedad()
        {
            return edad;
        }
        public void Setedad(int edad)
        {
            this.edad  = edad;
        }
        public string Getdepartamento()
        {
            return departamento;
        }
        public void Setdepartamento(string departamento)
        {
            this.departamento = departamento;
        }
        
        public void imprimir()
            {
                Console.WriteLine("Apellido:  " + apellido);
                Console.WriteLine("Nombre:  " + nombre);
                Console.WriteLine("Edad:  " + edad);
                Console.WriteLine("Departamento:  " + departamento);
            }

        
        
        }
    }


    

