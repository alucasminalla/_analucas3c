﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnaLucas3C
{
    class Empleado_por_hora : Empleado
    {
        public int num_horas_trabajadas;
        int precio_hora = 2;
        public Empleado_por_hora (int precio_hora, int num_horas_trabajadas, string apellido, string nombre, string departamento) : base(apellido, nombre, departamento)
        {
            this.num_horas_trabajadas = num_horas_trabajadas;
            this.precio_hora = precio_hora;

        }

        }
}
