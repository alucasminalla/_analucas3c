﻿using System;

namespace AnaLucas3C
{
    class Program
    {
        static void Main(string[] args)

        {
            Empleado empleado = new Empleado( "AGUILAR", "JOAHAN", "INFORMATICA");
            Console.WriteLine("LOS DATOS DEL EMPLEADO QUE SIRVE DE EJEMPLO SON: ");
            Console.WriteLine("EMPLEADO NUMERO 30 ");
            Console.WriteLine("APELLIDO: AGUILAR");
            Console.WriteLine("NOMBRE: JOAHAN");
            Console.WriteLine("DEPARTAMENTO: INFORMATICA");
            Console.WriteLine("");
            int opcion = 0;
            do
            {
                Console.WriteLine("INGRESE EL TIPO DE EMPLEADO QUE USTED ES:");
                Console.WriteLine("\n" +
                    "\n 1.-  EMPLEADO FIJO" +
                    "\n 2.-  EMPLEADO POR HORA" +
                    "\n 3.-  EMPLEADO TEMPORAL" +
                    "\n 4.-  CANCELAR BUSQUEDA \n ");
                Console.WriteLine("DIGITE UNA OPCION :");
                opcion = Convert.ToInt32(Console.ReadLine());
                switch (opcion)
                {
                    case 1 :

                        Empleado_fijo empleado_Fijo = new Empleado_fijo(2018, "AGUILAR", "JOAHAN", "INFORMATICA");
                        int sueldo_fijo = 500;
                        int bono_anual = 100;
                        int anio_ac = 2021;
                        int anio_entra;
                        int anio;
                        int sueldo;
                        Console.Write("INGRESE AÑO DE ENTRADA EN LA EMPRESA:...");
                        anio_entra = Convert.ToInt32(Console.ReadLine());
                        anio = anio_ac - anio_entra;
                      
                        sueldo = sueldo_fijo + (bono_anual * anio);
                        Console.Write("SU SUELDO ES DE:  $" + sueldo + "...");
                        Console.WriteLine("");
                        break;

                    case 2:

                        Empleado_por_hora empleado_por_hora = new Empleado_por_hora(2, 8, "AGUILAR", "JOAHAN", "INFORMATICA");
                        int horas_trabajadas_mes;
                        int precio_hora = 2;
                        int total;
                        Console.Write("INGRESE EL TOTAL DE HORAS TRABAJADAS EN EL MES:...");
                        horas_trabajadas_mes =Convert.ToInt32(Console.ReadLine());
                        total = horas_trabajadas_mes * precio_hora;
                        Console.Write("SU SUELDO ES DE:  $" + total + "...");
                        Console.WriteLine("");
                        break;

                    case 3:

                        Empleado_temporal empleado_temporal = new Empleado_temporal(400,2020,2021, "AGUILAR", "JOAHAN", "INFORMATICA");
                        int sueldo_fijo2= 400;
                        Console.Write("SU SUELDO ES DE:  $" + sueldo_fijo2 + "...");
                        Console.WriteLine("");
                        break;
                    case 4:
                        Console.WriteLine("SU BUSQUEDA HA SIDO CANCELADA");
                        Console.Clear();
                        break;
                    default:
                        break;
                        
                }

            } while (opcion!= 4);
        }
     
              
    }
}
