﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnaLucas3C
{
    class Empleado_temporal:Empleado
    {
        public int fecha_ingreso;
        public int fecha_salida;
        int sueldo = 500;

        public Empleado_temporal(int sueldo, int fecha_ingreso, int fecha_salida, string apellido, string nombre, string departamento) : base(apellido, nombre, departamento)
        {
            this.fecha_ingreso = fecha_ingreso;
            this.fecha_salida = fecha_salida;
            this.sueldo = sueldo;

        }
     

    }
}
